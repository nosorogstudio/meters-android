package com.example.meters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.meters_data.view.*


class MainAdapter : RecyclerView.Adapter<CustomViewHolder>() {


    private val metersTypes = listOf(
        MeterResult("Электричество", 0, ButtonType.MoveToApp, R.drawable.flash, false),
        MeterResult("Холодная вода", 1, ButtonType.SendByEmail, R.drawable.cold_water, false),
        MeterResult("Горячая вода", 2, ButtonType.MoveToApp, R.drawable.hot_water, false),
        MeterResult("Газ", 3, ButtonType.Sent, R.drawable.gas, true)
    )


    override fun getItemCount(): Int {
        return metersTypes.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.meters_data, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val item = metersTypes[position]


        holder.view.tv_meter_type.text = metersTypes[position].name
        holder.view.iv_meter_image.setImageResource(metersTypes[position].imageId)
        val isSent = item.isSent
        if (isSent) {
            bindAsSent(holder.view.btn_send_data, metersTypes[position].buttonType.type)
        } else {
            bindAsNotSent(holder.view.btn_send_data, metersTypes[position].buttonType.type)
        }
        holder.view.tv_meter_data.text = metersTypes[position].metersData.toString()
        holder.view.btn_send_data.setOnClickListener {
            Toast.makeText(holder.view.context, metersTypes[position].name, Toast.LENGTH_SHORT).show()
        }


    }

    private fun bindAsNotSent(button: Button, buttonText: String) {
        button.text = buttonText
        button.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        button.setBackgroundResource(R.drawable.button_background)
        button.setTextColor(button.context.resources.getColorStateList(R.color.button_text))
    }

    private fun bindAsSent(button: Button, buttonText: String) {
        val image = button.context.resources.getDrawable(R.drawable.ic_done, null)
        button.text = buttonText
        val btnTextColor = button.context.resources.getColorStateList(R.color.button_text_sent)


        button.setCompoundDrawablesWithIntrinsicBounds(image, null, null, null)
        button.setBackgroundResource(R.drawable.button_background)
        button.setTextColor(btnTextColor)
        image.setTintList(btnTextColor)

    }


}


class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view)