package com.example.meters

data class MeterResult(
    val name: String, var metersData: Int,
    val buttonType: ButtonType, val imageId: Int, var isSent: Boolean
)

enum class ButtonType(var type: String) {
    MoveToApp("ПЕРЕЙТИ В ПРИЛОЖЕНИЕ"),
    SendByEmail("ОТПРАВИТЬ ПО ПОЧТЕ"),
    Sent("ОТПРАВЛЕНО")
}